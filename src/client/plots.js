// function fetchAndVisualizeData() {
//     fetch("/home/happy/Desktop/testing the project/src/public/output/matchesPerYear.json")
//         .then(r => r.json())
//         .then(visualizeData);
// }

// fetchAndVisualizeData();

// function visualizeData(data) {
//     visualizeMatchesPlayedPerYear(data.matchesPerYear);
//     return;
// }

// function visualizeMatchesPlayedPerYear(matchesPerYear) {
//     const seriesData = [];
//     for (let year in matchesPerYear) {
//         seriesData.push([year, matchesPerYear[year]]);
//     }

//     Highcharts.chart("matchesPlayedPerYear", {
//         chart: {
//             type: "column"
//         },
//         title: {
//             text: "Matches Played Per Year"
//         },

//         xAxis: {
//             type: "category"
//         },
//         yAxis: {
//             min: 0,
//             title: {
//                 text: "Matches"
//             }
//         },
//         series: [
//             {
//                 name: "Years",
//                 data: seriesData
//             }
//         ]
//     });
// }
//document.addEventListener('DOMContentLoader', () => {


//})
Highcharts.chart('matchesPlayedPerYear', {
    chart: {
        type: 'column'
    },
    credits:
    {
        enabled: false
    },
    title: {
        text: 'Matches Played Per Year'
    },

    xAxis: {
        type: 'category',

    },
    yAxis: {
        min: 0,
        title: {
            text: 'Number of matches'
        }
    },
    legend: {
        enabled: false
    },

    series: [{
        name: 'Matches',
        data: [

            ["2008", 58],
            ["2009", 57],
            ["2010", 60],
            ["2011", 73],
            ["2012", 74],
            ["2013", 76],
            ["2014", 60],
            ["2015", 59],
            ["2016", 60],
            ["2017", 59]
        ],

    }]
});


Highcharts.chart('extrarunsPerTeamIn2016', {

    chart: {
        type: 'column'
    },
    credits:
    {
        enabled: false
    },
    title: {
        text: 'Extra Runs Per team in 2016'
    },

    xAxis: {
        type: 'category',

    },
    yAxis: {
        min: 0,
        title: {
            text: 'Extra Runs Per Team'
        }

    },
    legend: {
        enabled: false
    },

    series: [{
        name: 'ExtraRuns',
        data: [
            ["Mumbai Indians", 102,],
            ["Rising Pune Supergiants", 101],
            ["Delhi Daredevils", 109],
            ["Kolkata Knight Riders", 130],
            ["Kings XI Punjab", 83],
            ["Gujarat Lions", 132],
            ["Royal Challengers Bangalore", 118],
            ["Sunrisers Hyderabad", 124]

        ],

    }]
});


Highcharts.chart('top10EconomicalBowlersIn2015', {
    chart: {
        type: 'column'
    },
    credits:
    {
        enabled: false
    },
    title: {
        text: 'Top 10 Economical Bowlers In 2015'
    },

    xAxis: {
        type: 'category',

    },
    yAxis: {
        min: 0,
        title: {
            text: 'Scores'
        }
    },
    legend: {
        enabled: false
    },

    series: [{
        name: 'Top10',
        data: [

            ["RN ten Doeschate", 3.4285714285714284],
            ["J Yadav", 4.142857142857143],
            ["V Kohli", 5.454545454545455],
            ["R Ashwin", 5.725],
            ["S Nadeem", 5.863636363636364],
            ["Z Khan", 6.15483870967742],
            ["Parvez Rasool", 6.2],
            ["MC Henriques", 6.2675159235668785],
            ["MA Starc", 6.75],
            ["M de Lange", 6.923076923076923]

        ],

    }]
});


Highcharts.chart('matchesWonPerTeamPerYear', {
    chart: {
        type: 'column'
    },
    credits:
    {
        enabled: false
    },
    title: {
        text: 'Matches Won Per Team Per Year'
    },

    xAxis: {

        type: 'category',
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Number of matches'
        }
    },
    legend: {
        enabled: false
    },

    series: [{
        name: 2008,
        data: [
            ["Kolkata Knight Riders", 6], ["Chennai Super Kings", 9], ["Delhi Daredevils", 7],
            ["Royal Challengers Bangalore", 4],
            ["Rajasthan Royals", 13], ["Kings XI Punjab", 10],
            ["Deccan Chargers", 2], ["Mumbai Indians", 7]
        ]



    },
    {
        name: 2009,
        data: [["Mumbai Indians", 5], ["Royal Challengers Bangalore", 9],
        ["Delhi Daredevils", 10], ["Deccan Chargers", 9], ["Chennai Super Kings", 8],
        ["Kolkata Knight Riders", 3], ["Rajasthan Royals", 6], ["Kings XI Punjab", 7]
        ]



    },
    {
        name: 2010,
        data: [["Kolkata Knight Riders", 7], ["Mumbai Indians", 11], ["Delhi Daredevils", 7],
        ["Deccan Chargers", 8], ["Royal Challengers Bangalore", 8], ["Chennai Super Kings", 9],
        ["Rajasthan Royals", 6], ["Kings XI Punjab", 4]]



    },
    {
        name: 2011,
        data: [["Chennai Super Kings", 11],
        ["Rajasthan Royals", 6], ["Royal Challengers Bangalore", 10], ["Mumbai Indians", 10],
        ["Pune Warriors", 4], ["Kolkata Knight Riders", 8], ["Kings XI Punjab", 7],
        ["Deccan Chargers", 6], ["Kochi Tuskers Kerala", 6], ["Delhi Daredevils", 4]
        ]



    },]
});

