import fs from 'fs'
import path from 'path'
import { fileURLToPath } from 'url'
import ipl from './ipl.js'

const __dirname = path.dirname(fileURLToPath(import.meta.url))
console.log(__dirname)
const deliveriesJsonPath = path.resolve(__dirname, "../data/deliveries.json");
const matchesJsonPath = path.resolve(__dirname, "../data/matches.json");
console.log(matchesJsonPath)

// const { matchesPerYear, matchesWonPerTeam,
//     extraRunsPerTeamInYear,
//     top10EconomicalBowlersInYear, totalBallsThrownByRcb } = require('./ipl');
//console.log('ok')
const deliveriesJson = JSON.parse(fs.readFileSync(deliveriesJsonPath));
const matchesJson = JSON.parse(fs.readFileSync(matchesJsonPath));


let matchesPerYr = ipl.matchesPerYear(matchesJson)
let matchesWonPerTm = ipl.matchesWonPerTeam(matchesJson)
let extraRunsPerTeamInYr = ipl.extraRunsPerTeamInYear(matchesJson, deliveriesJson, 2016)
let top10EconomicalBowlersInYr = ipl.top10EconomicalBowlersInYear(matchesJson, deliveriesJson, 2015)


function outputSaver(outputFilePath, outputJson) {
    const outputPath = "../public/output";

    fs.writeFileSync(path.resolve(__dirname, outputPath, `${outputFilePath}.json`), JSON.stringify(outputJson), "utf-8", (err) => {
        if (err) { console.log(err); }
    });
}

outputSaver("matchesPerYear", matchesPerYr);
outputSaver("matchesWonPerTeam", matchesWonPerTm);
outputSaver("extraRunPerTeam2016", extraRunsPerTeamInYr);
outputSaver("economicalBowlers2015", top10EconomicalBowlersInYr);
//outputSaver("totalBallsThrownByRcb", totalBalls)