
import path from 'path'
import { fileURLToPath } from 'url'
import fs from 'fs'
import CSVtoJSON from 'csvtojson'

const __dirname = path.dirname(fileURLToPath(import.meta.url))

const MATCHES_CSV_FILE = '/home/happy/Desktop/testing the project/src/data/matches.csv';
const DELIVERIES_CSV_FILE = '/home/happy/Desktop/testing the project/src/data/deliveries.csv';

const modifyOutputFilePath = (path) => path.replace('.csv', '.json');

function csvToJsonConverter(inputCsvPath) {
    CSVtoJSON()
        .fromFile(path.resolve(__dirname, inputCsvPath))
        .then((json) => {
            fs.writeFileSync(path.resolve(__dirname, modifyOutputFilePath(inputCsvPath)), JSON.stringify(json), "utf-8", (err) => {
                if (err) { console.log(err); }
            })
        })
}


csvToJsonConverter(MATCHES_CSV_FILE);
csvToJsonConverter(DELIVERIES_CSV_FILE);