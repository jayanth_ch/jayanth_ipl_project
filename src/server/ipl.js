// one
function matchesPerYear(matches) {
    let result1 = {};

    matches.forEach((match) => {
        let season = match.season;
        if (result1[season]) {
            result1[season] += 1;
        } else {
            result1[season] = 1;
        }
    })
    return result1;
}

// two
function matchesWonPerTeam(matches) {
    let result2 = {};

    matches.forEach((match) => {
        let season = match.season;
        let winner = match.winner;
        if (result2[season]) {
            if (result2[season][winner]) {
                result2[season][winner] += 1;
            } else {
                result2[season][winner] = 1;
            }
        } else {
            result2[season] = {};
            result2[season][winner] = 1;
        }
    })
    return result2;
}


// three
function extraRunsPerTeamInYear(matches, deliveries, year) {

    let result3 = {};

    matches.forEach(match => {

        if (match.season == year) {

            deliveries.forEach(delivery => {

                if (match.id == delivery.match_id) {

                    if (result3[delivery.batting_team]) {
                        result3[delivery.batting_team] += Number(delivery.extra_runs);
                    }
                    else {
                        result3[delivery.batting_team] = Number(delivery.extra_runs);
                    }
                }
            });

        }

    });

    return result3;

}



// four
function top10EconomicalBowlersInYear(matches, deliveries, year) {
    let bowlerData = {};
    let topBowlers = [];
    let result4 = {};
    matches.filter(match => match.season == year).forEach(match => {

        deliveries.filter(delivery => match.id == delivery.match_id).forEach(delivery => {

            if (bowlerData[delivery.bowler]) {

                bowlerData[delivery.bowler].balls += 1;
                bowlerData[delivery.bowler].total_runs += Number(delivery.total_runs);

            } else {

                bowlerData[delivery.bowler] = {};
                bowlerData[delivery.bowler].total_runs = Number(delivery.total_runs);
                bowlerData[delivery.bowler].balls = 1;

            }
        });
    });

    for (let bowler in bowlerData) {

        let overs = (bowlerData[bowler].balls) / 6;
        let runs = bowlerData[bowler].total_runs;
        let economyRate = runs / overs;

        topBowlers.push([bowler, economyRate]);

    }

    topBowlers.sort((a, b) => a[1] - b[1]);
    (topBowlers.slice(0, 10)).forEach(item => {
        result4[item[0]] = item[1];
    });

    return result4;

}

//five
//total no.of balls thrown by rcb in 2015
function totalBallsThrownByRcb(matches, deliveries, year, bt) {
    let result4 = {};

    matches.forEach(match => {

        if (match.season == year) {

            deliveries.forEach(delivery => {

                if (match.id == delivery.match_id && delivery.bowling_team == bt) {

                    if (result4[delivery.bowling_team]) {
                        result4[delivery.bowling_team] += Number(delivery.ball);
                    }
                    else {
                        result4[delivery.bowling_team] = Number(delivery.ball);
                    }
                }
            });

        }

    });

    return result4;


}
// module.exports = {
//     matchesPerYear, matchesWonPerTeam,
//     extraRunsPerTeamInYear, top10EconomicalBowlersInYear, totalBallsThrownByRcb
// }
export default
    {
        matchesPerYear, matchesWonPerTeam,
        extraRunsPerTeamInYear, top10EconomicalBowlersInYear, totalBallsThrownByRcb
    }



